# TechExpertsPH - SMS Notification System
#### Client-side Server

> [Jhon Andrew Q. Baes](https://fb.me/mongsangga) | <anecbook@gmail.com> / [Messenger](https://m.me/mongsangga)

* * *

##### Setup
1. First, install the dependencies.
	* `npm install`
2. Test the server.
	* `npm test`

#### System Flow
1. Check biometrics DB for un-notified entries. Limit by 10 results per query.
2. If no results returned, wait 10 seconds then back to Step 1.
3. If results returned, get USERINFO of each result.
4. Backup data to NeDB.
5. Send formatted data to Firebase.
6. Set CHECKINOUT data as notified.
7. After processing all returned results, restart flow.

#### Sample data being sent

```
{
	"schoolId": "wmsu",
	"studentId": 1,
	"studentName": "Jhon Andrew Baes",
	"contactNo": "09055754790",
	"checkTime": "2016-07-01T00:27:45Z",
	"checkType": "I", // "I" for TIME IN, "O" for TIME OUT
	"status": "sending"
}
```

#### Note

* MSAccess doesn't return expected results when performing JOIN statements, so I query TimeIO entries first then get its user information.
* Since `CHECKINOUT` table doesn't have a unique key for referencing, I added `id` column.
* I also added `notified` column to identify when the data was already processed.