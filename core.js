// Dependencies
const firebaseAdmin	= require('firebase-admin');

// Dependency Initializations
const licenseId = require('./credentials/license').id.toLowerCase();

firebaseAdmin.initializeApp({
	credential: firebaseAdmin.credential.cert(require('./credentials/puy9e-f1c205e494')),
	databaseURL: "https://techexperts-sms-gateway.firebaseio.com",
	databaseAuthVariableOverride: {
		uid: licenseId
	}
});

// Libraries
const
	Query		= require('./libs/query'),
	Backup		= require('./libs/backup'),
	util		= require('./libs/utilities')
;

// Main Algorithm

const firebaseDb	= firebaseAdmin.database().ref(licenseId);
let checkTimeout	= 10;
let maxTimeout		= 20;

// Define Cron Job
let startCron = () => {

	// Query for un-notified entries. Limit results by 10
	Query([
		'SELECT TOP 10',
			'id AS entryId,',
			'USERID AS studentId,',
			'CHECKTIME AS checkTime,',
			'CHECKTYPE AS checkType',
		'FROM CHECKINOUT',
		'WHERE notified = 0',
		'ORDER BY CHECKTIME DESC'
	].join(" ")).then(resp => {

		if(resp.records.length === 0) {
			// If no results returned...
			util.log('SYSTEM', `No pending records. Waiting for ${checkTimeout} seconds.`);

			// Wait for given timeout
			setTimeout(() => {
				// If timeout is not yet at max, increment by 5
				if(checkTimeout < maxTimeout) checkTimeout += 5;
				// Start cron again
				startCron();
			}, checkTimeout * 1000);
		} else {

			// If there are returned results...
			util.log('QUERY', `Query success. ${resp.records.length} results returned.`);

			// Define how many results returned
			let recordLength = resp.records.length;

			// Loop to each result
			resp.records.forEach((record, index) => {

				// Get data of the USERID from USERINFO table
				Query(`SELECT Name AS studentName, OPHONE AS contactNo FROM USERINFO WHERE USERID = ${record.studentId}`).then(resp => {

					// Join the timeIO data and the userinfo in one variable
					let data = Object.assign(record, resp.records[0]);

					// Backup data locally
					Backup(data).then(backupData => {

						util.log(`NeDB`, `Data has been backed up locally. ID #${record.entryId}`);

						// Send data to firebase
						firebaseDb.push().set(backupData).then(() => {
							util.log(`Firebase`, `Data has been sent. ID #${record.entryId}`);
						});

						// Update the timeIO data as notified
						Query(`UPDATE CHECKINOUT SET notified = true WHERE id = ${record.entryId} AND USERID = ${backupData.studentId}`, true).then(resp => {
							util.log('MSAccess', 'TimeIO data has been set to notified.');

							// If current data is the last one from the set of results
							if(index === (recordLength - 1)) {
								// Set timeout back to 10
								checkTimeout = 10;
								util.log('SYSTEM', 'checkTimeout reset.');
								// Start cron again
								startCron();
							}

						}).catch(resp => util.log('MSAccess', 'Set to notified failed.', 'error')); // Error updating TimeIO as notified

					}).catch(resp => console.log('NeDB', 'Backup failed', 'error')); // Error backing up locally
				}).catch(resp => console.log('MSAccess', 'Getting user info failed.', 'error')); // Error getting user info
			});

		}

	}).catch(resp => {
		// Error getting un-notified data
		util.log('MSAccess', 'Error getting un-notified data', 'error');
	});

};

// Initialize cron job
startCron();