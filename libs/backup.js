// Dependencies
const nedb		= require('nedb');

// Database Initialization
const dbDir		= './database/backup.db';
const backup	= new nedb(dbDir);

module.exports = data => {
	return new Promise((resolve, reject) => {

		// Load Database
		backup.loadDatabase(err => {
			if(err) throw err;

			// Format Data
			let formattedData = {
				"schoolId": require('../credentials/license').id,
				"studentId": data.studentId,
				"studentName": data.studentName,
				"contactNo": data.contactNo,
				"checkTime": data.checkTime,
				"checkType": (data.checkType == "I") ? "in" : "out",
				"status": "sending"
			};

			// Insert Data to Backup
			backup.insert(formattedData, (err, doc) => {
				if(err) reject({code: "backup_error", data: err});
				else {
					delete doc._id;
					resolve(doc);
				}
			});
		});

	});
}