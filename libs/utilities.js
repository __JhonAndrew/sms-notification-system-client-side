const debugMode	= true;
const winston	= require('winston');

let logger = new (winston.Logger)({
	transports: [
		new (winston.transports.Console)({
			level: (debugMode) ? 'info' : 'important',
			colorize: true,
			timestamp: () => {
				return Date.now();
			},
			formatter: (options) => {
				return `${new Date(options.timestamp()).toLocaleTimeString()} ${winston.config.colorize(options.level, options.level.toUpperCase())} [${options.meta.ref}]: ${options.message}`;
			}
		}),
		new (winston.transports.File)({
			level: 'info',
			filename: 'system.log'
		})
	],
	levels: {
		error: 0,
		important: 1,
		info: 2
	},
	colors: {
		error: 'red',
		important: 'green',
		info: 'blue'
	}
});

module.exports = {
	log(by, msg, type = 'info') {
		logger.log(type, msg, {ref: by});
	},
	rand(min, max) { return Math.floor(Math.random() * (max - min + 1)) + min; }
};