// Dependencies
const adodb		= require('node-adodb');

// Database Initialization
const dbDir		= './database/attBackup.mdb';
const msaccess	= adodb.open('Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+dbDir+';');

module.exports = (query, exec = false) => {
	return new Promise((resolve, reject) => {
		((exec) ? msaccess.execute(query) : msaccess.query(query))
			.on('done', resp => {
				if(resp.valid) resolve(resp);
				else reject({code: 'unexpected_result', resp: resp});
			})
			.on('fail', resp => reject({code: "query_failed", resp: resp}))
		;
	});
};